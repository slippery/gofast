
<figure style="text-align: center;">
    <img src="images/gofast.png" width=345pt>
</figure>



# gofast

gofast 是一个为 Go 语言开发者设计的实用工具包，旨在提供一系列方便、易用的操作函数，以帮助快速开发 Go 应用。该包包含了多种常用操作，如字符串处理、JSON 文件读写、日志记录等，适用于各种 Go 项目。

功能特点
- 字符串处理：提供方便的字符串操作函数，如格式化、拼接等。
- JSON 操作：简化 JSON 文件的读取和写入操作。
- 日志记录：提供简单而强大的日志记录功能。
- 更多实用工具：包括但不限于文件操作、数据转换等。

# 安装

确保你已经安装了 Go (版本 1.15 或更高)。你可以通过运行以下命令来安装 gofast：

```bash
go get github.com/ultrasev/gofast
```

# 快速开始
以下是如何在你的项目中使用 gofast 的一个简单示例：

```go
package main

import (
    "github.com/ultrasev/gofast/io"
)

func main() {
    // 使用 io 包中的 Puts 函数打印字符串
    io.Puts("Hello, gofast!")
    
    // 读取 JSON 文件
    data, err := io.JsonLoad("/path/to/your/file.json")
    if err != nil {
        panic(err)
    }
    io.Puts(data)
}

```


# 贡献
gofast 欢迎任何形式的贡献，无论是新功能、bug 修复还是文档改进。如果你想为项目贡献代码，请遵循以下步骤：

- Fork 仓库。创建一个新的分支 (git checkout -b feature-branch)。
- 提交你的更改 (git commit -am 'Add some feature')。
- 推送到分支 (git push origin feature-branch)。
- 创建新的 Pull Request。

# 许可证

本项目采用 MIT 许可证。详情见 [LICENSE](./LICENCE) 文件。

# 联系方式
如有任何问题或建议，请通过 GitHub issues 提交。

