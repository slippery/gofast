package main

import (
	// "gitlab.com/slippery/gofast/io"
	"gofast/io"
	"gofast/log"
	"math"
	// "gitlab.com/slippery/gofast/io"
)

func main() {
	log.InitLogger("/tmp/gofast.log")
	v := math.Max(2, 3)
	io.Logger.Info(v)
	for i := 0; i < 1; i++ {
		log.Info(i)
		log.Info("Hello, World!")
		log.Info("Hello, again!")
	}
}
