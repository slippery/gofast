package io

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"
)

const defaultUserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36"

type WriteCounter struct {
	Total    uint64
	Start    time.Time
	FileSize int64
}

func (wc *WriteCounter) Write(p []byte) (int, error) {
	n := len(p)
	wc.Total += uint64(n)
	elapsed := time.Since(wc.Start).Seconds()
	fmt.Printf("\rDownloaded %.2f MB (%.2f%%) at %.2f MB/s", float64(wc.Total)/1024/1024, float64(wc.Total)*100/float64(wc.FileSize), float64(wc.Total)/(elapsed*1024*1024))
	return n, nil
}

func DownloadFile(url string, path string, retry int) error {
	if _, err := os.Stat(path); err == nil {
		fmt.Println("File already exists, skipping download")
		return nil
	}

	fmt.Println(fmt.Sprintf("Downloading %s to %s", url, path))
	client := &http.Client{
		Timeout: time.Second * 3600,
	}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}

	req.Header.Set("User-Agent", defaultUserAgent)
	retryFunc := func() error {
		resp, err := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		fileSize, err := strconv.ParseInt(resp.Header.Get("Content-Length"), 10, 64)
		if err != nil {
			fmt.Println("Error:", err)
			return err
		}

		out, err := os.Create(path)
		if err != nil {
			return err
		}
		defer out.Close()

		counter := &WriteCounter{Start: time.Now(), FileSize: fileSize}
		if _, err = io.Copy(out, io.TeeReader(resp.Body, counter)); err != nil {
			return err
		}
		fmt.Println("\nDownload successful, file saved to %s", path)
		return nil
	}
	for i := 0; i < retry; i++ {
		if err := retryFunc(); err == nil {
			return nil
		}
		fmt.Println("retrying after 1 second")
		time.Sleep(1 * time.Second)
	}

	return fmt.Errorf("failed to download after %d retries", retry)
}
