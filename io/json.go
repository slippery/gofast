package io

import (
	"encoding/json"
	"fmt"
	"os"
)

func JsonLoad(filePath string) (interface{}, error) {

	fileContent, err := os.ReadFile(filePath)
	if err != nil {
		return nil, fmt.Errorf("error reading the file: %w", err)
	}

	var data interface{}
	err = json.Unmarshal(fileContent, &data)
	if err != nil {
		return nil, fmt.Errorf("error parsing JSON: %w", err)
	}
	return data, nil
}
