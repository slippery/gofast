package io

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"sort"

	"github.com/sirupsen/logrus"
)

var Logger *logrus.Logger

func init() {
	Logger = logrus.New()
	Logger.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})
	Logger.SetOutput(os.Stdout)

}

func Puts(a interface{}) {
	// 使用类型切换来确定 a 的类型
	switch v := a.(type) {
	case map[interface{}]interface{}:
		for key, value := range v {
			fmt.Printf("%v : %v\n", key, value)
		}
	case map[string]interface{}:
		prettyJson, _ := json.MarshalIndent(v, "", "  ")
		fmt.Println(string(prettyJson))

	default:
		// 对于所有其他类型，使用标准的打印逻辑
		fmt.Println(a)
	}
}

func B64encode(v string) string {
	return base64.StdEncoding.EncodeToString([]byte(v))
}

func B64decode(v string) string {
	data, _ := base64.StdEncoding.DecodeString(v)
	return string(data)
}

func Shuffle(s string) string {
	runes := []rune(s)
	for i := range runes {
		j := rand.Intn(i + 1)
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func SortString(w string) string {
	cs := []rune(w)
	sort.Slice(cs, func(i, j int) bool {
		return cs[i] < cs[j]
	})
	return string(cs)
}

// create a map of all chars
const chars = "hR$1&daLW40u6NIc}siz{p%!]3?(Ayg+mZS>E`8lPv|YJ@9#~bKr.D,5MwQe;[q-*C^TOt)VG=Bk27:fjx_<oUXHnF"

func Encrypt(s string) string {
	// sort chars first and create a map for each char
	sortedChars := SortString(chars)
	charMap := make(map[string]string)
	for i, c := range sortedChars {
		charMap[string(c)] = string(chars[i])
	}
	// encrypt the string
	encrypted := ""
	for _, c := range s {
		encrypted += charMap[string(c)]
	}
	return encrypted
}

func Decrypt(s string) string {
	// sort chars first and create a map for each char
	sortedChars := SortString(chars)
	charMap := make(map[string]string)
	for i, c := range sortedChars {
		charMap[string(chars[i])] = string(c)
	}
	// decrypt the string
	decrypted := ""
	for _, c := range s {
		decrypted += charMap[string(c)]
	}
	return decrypted
}
