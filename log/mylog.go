package log

import (
	"os"

	"github.com/sirupsen/logrus"
)

var Logger *logrus.Logger
var LoggerF *logrus.Logger

func init() {
	Logger = logrus.New()
	Logger.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})
	Logger.SetOutput(os.Stdout)
}

func InitLogger(logfile string) {
	LoggerF = logrus.New()
	LoggerF.SetFormatter(&logrus.TextFormatter{
		FullTimestamp:    true,
		DisableTimestamp: false,
	})
	file, err := os.OpenFile(logfile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err == nil {
		LoggerF.SetOutput(file)
	} else {
		LoggerF.Info("Failed to log to file, using default stderr")
	}
}

func Info(a ...interface{}) {
	Logger.Info(a...)
	if LoggerF == nil {
		return
	}
	LoggerF.Info(a...)
}

func Error(a ...interface{}) {
	Logger.Error(a...)
	if LoggerF == nil {
		return
	}
	LoggerF.Error(a...)
}

func Warn(a ...interface{}) {
	Logger.Warn(a...)
	if LoggerF == nil {
		return
	}
	LoggerF.Warn(a...)
}

func Debug(a ...interface{}) {
	Logger.Debug(a...)
	if LoggerF == nil {
		return
	}
	LoggerF.Debug(a...)
}
