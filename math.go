package math

func Max(a, b interface{}) interface{} {
	switch a.(type) {
	case int:
		if a.(int) > b.(int) {
			return a
		}
		return b
	case float64:
		if a.(float64) > b.(float64) {
			return a
		}
		return b
	case bool:
		if a.(bool) {
			return a
		}
		return b
	default:
		panic("unsupported type")
	}
}
